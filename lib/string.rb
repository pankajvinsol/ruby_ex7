# Public: Override default implementation of to_s so that it swap case of line
#
# Examples
#
#   "AaBb".to_s
#   # => aAbB
#
# Returns the duplicated String.

class String
  def to_s
    swapcase
  end
end
